# from palantir.i3ipc._attrs import FromDictMixin
import attr


class FromDictMixin:
    @classmethod
    def from_dict(cls, data):
        return cls(data)


class Foo(FromDictMixin):
    def __init__(self, data):
        self.data = data


@attr.s
class Bar:
    name: int = attr.ib()


@attr.s
class Bar2(FromDictMixin):
    name: int = attr.ib()

    # @classmethod
    # def from_dict(cls, data):
    #     return cls(data)
