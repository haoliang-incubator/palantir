from palantir.i3ipc import Variant, reply_types
from palantir.rice import iconizing


def test_funcs(data_loader):
    data = data_loader("get_tree.reply.i3")
    variant = Variant.I3

    root_tree = reply_types.Tree.from_dict(data)
    wptrees = list(iconizing.wptrees_in_root_tree(root_tree))

    for wptree in wptrees:
        assert isinstance(wptree, reply_types.Tree)

    for wptree in wptrees:
        progs = iconizing.programs_in_wptree(wptree, variant)
        for prog in progs:
            assert isinstance(prog, str)


def test_types_creation(data_loader):
    data = data_loader("get_tree.reply.i3")
    variant = Variant.I3

    root_tree = reply_types.Tree.from_dict(data)

    root = iconizing.Root.from_root_tree(root_tree, variant)

    for wp in root.workspaces:
        assert isinstance(wp, iconizing.Workspace)
        assert wp.iconized
