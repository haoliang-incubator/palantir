import json
from pathlib import Path

import pytest


@pytest.fixture()
def testdata_dir():
    return Path(__file__).resolve().parent.joinpath("testdata")


@pytest.fixture()
def data_loader(testdata_dir: Path):
    def loader(filename: str):
        file = testdata_dir.joinpath(filename)
        with open(file, "rb") as fp:
            return json.load(fp)

    return loader
