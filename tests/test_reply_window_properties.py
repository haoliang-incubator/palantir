import pytest
from palantir.i3ipc.reply_types import _container


def test_firefox_dialog():
    reply = {
        "class": "firefox",
        "instance": "Firefox",
        "window_role": "Dialog",
        "transient_for": 25166154,
    }

    winprop = _container._WindowProperties.from_dict(reply)

    assert winprop.title is None


def test_invalid_vivaldi_winprop():
    reply = {"title": "Vivaldi", "transient_for": None}
    # how can a program in x11 have no class!

    with pytest.raises(ValueError):
        _container._WindowProperties.from_dict(reply)


def test_invalid_xev_winprop():
    reply = {"title": "Event Tester", "transient_for": None}
    # how can a program in x11 have no class!

    with pytest.raises(ValueError):
        _container._WindowProperties.from_dict(reply)


def test_invalid_i3nagbar_winprop():
    reply = {"transient_for": None}
    # how can a program in x11 have no class!

    with pytest.raises(ValueError):
        _container._WindowProperties.from_dict(reply)
