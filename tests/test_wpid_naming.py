# pylint: disable=broad-except

from palantir.rice import naming


def test_extract_wpno():
    feed = [
        # valid forms
        ("0:browser", "0"),
        ("b:browser", "b"),
        ("0:浏览器", "0"),
        ("b:浏览器", "b"),
        ("0", "0"),
        # invalid forms
        ("-1", ValueError),
        ("00", ValueError),
        ("b", ValueError),
        ("00:browser", ValueError),
        ("browser", ValueError),
        ("-1:browser", ValueError),
        ("浏览器", ValueError),
    ]

    for wpid, expect in feed:
        try:
            wpno = naming.extract_wpno(wpid)
        except Exception as e:
            assert isinstance(e, expect)
        else:
            assert wpno == expect


def test_allocate_wpid():
    feed = [
        # wpids, curwpid, given_name, expect
        ((), "0", "browser", "0:browser"),
        ((), "0", "0:browser", "0:browser"),
        ((), "0:browser", "browser-s", "0:browser-s"),
        ((), "b:browser", "browser-s", "b:browser-s"),
        (("0:browser",), "0:browser", "b:browser", "b:browser"),
        (("0:browser",), "0:browser", "0", "0:browser"),
        (("0:browser",), "0:browser", "b:terminal", "b:terminal"),
        (("0:browser", "1:terminal"), "1:terminal", "0:terminal", RuntimeError),
    ]

    for wpids, curwpid, given_name, expect in feed:
        try:
            allocated = naming.allocate_wpid(given_name, curwpid, wpids)
        except Exception as e:
            assert isinstance(e, RuntimeError)
        else:
            assert allocated == expect
