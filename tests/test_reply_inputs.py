from palantir.i3ipc.reply_types import _input


def test_input_from_dict(data_loader):
    data = data_loader("get_inputs.reply.sway.crafted")

    inputs = [_input.input_from_dict(input_) for input_ in data]

    touchpad: _input.Touchpad = inputs[0]
    assert isinstance(touchpad, _input.Touchpad)
    assert isinstance(touchpad.libinput, _input._LibinputTouchpad)

    keyboard: _input.Keyboard = inputs[1]
    assert isinstance(keyboard, _input.Keyboard)
    assert isinstance(keyboard.libinput, _input._Libinput)

    switch: _input.Keyboard = inputs[4]
    assert isinstance(switch, _input.Switch)
    assert isinstance(switch.libinput, _input._Libinput)

    assert len(_input.Input.variants) == 3
