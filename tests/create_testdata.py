import json

import trio
from palantir import i3ipc
from palantir.i3ipc.protocol import MessageType

feed = [
    (MessageType.COMMAND, "command.reply.sway", b"nop; xxx"),
    (MessageType.GET_WORKSPACES, "get_workspaces.reply.sway", b""),
    (MessageType.SUBSCRIBE, "subscribe.reply.sway", b'["workspace"]'),
    (MessageType.GET_OUTPUTS, "get_outputs.reply.sway", b""),
    (MessageType.GET_TREE, "get_tree.reply.sway", b""),
    (MessageType.GET_MARKS, "get_marks.reply.sway", b""),
    (MessageType.GET_BAR_CONFIG, "get_bar_config.reply.sway", b""),
    (MessageType.GET_VERSION, "get_version.reply.sway", b""),
    (MessageType.GET_BINDING_MODES, "get_binding_modes.reply.sway", b""),
    (MessageType.GET_CONFIG, "get_config.reply.sway", b""),
    # (MessageType.SEND_TICK, "send_tick.reply.sway", b""),
    (MessageType.GET_BINDING_STATE, "get_binding_state.reply.sway", b""),
    (MessageType.GET_INPUTS, "get_inputs.reply.sway", b""),
    (MessageType.GET_SEATS, "get_seats.reply.sway", b""),
]


async def get_and_dump(
    conn: i3ipc.Connection, lock: trio.Lock, mt: MessageType, file, payload: bytes
):
    async with lock:
        resp = await conn.round_trip(mt, payload)

    async with await file.open("wb") as fp:
        content = json.dumps(resp)
        await fp.write(content.encode())


async def main():
    conn = await i3ipc.Connection.from_local()
    lock = trio.Lock()
    root = trio.Path(__file__).parent.joinpath("tests", "testdata")

    async with conn:
        async with trio.open_nursery() as nursery:
            for mt, file, payload in feed:
                nursery.start_soon(
                    get_and_dump, conn, lock, mt, root.joinpath(file), payload
                )


if __name__ == "__main__":
    trio.run(main)
