from palantir.rice import navigating


def test_next_wpid():
    feed = [
        # wpids, curwpid, cyclic, expect
        # number
        (("0:browser", "1:terminal", "3:mplayer"), "0:browser", True, "1:terminal"),
        (("0:browser", "1:terminal", "3:mplayer"), "1:terminal", True, "3:mplayer"),
        (("0:browser", "1:terminal", "3:mplayer"), "3:mplayer", True, "0:browser"),
        # number + letter
        (("0:browser", "1:terminal", "a:mplayer"), "0:browser", True, "1:terminal"),
        (("0:browser", "1:terminal", "a:mplayer"), "1:terminal", True, "a:mplayer"),
        (("0:browser", "1:terminal", "a:mplayer"), "a:mplayer", True, "0:browser"),
    ]

    for wpids, curwpid, cyclic, expect in feed:
        nextwpid = navigating.next_wpid(curwpid, wpids, cyclic)
        assert nextwpid == expect


def test_prev_wpid():
    feed = [
        # wpids, curwpid, cyclic, expect
        # number
        (("0:browser", "1:terminal", "3:mplayer"), "0:browser", True, "3:mplayer"),
        (("0:browser", "1:terminal", "3:mplayer"), "1:terminal", True, "0:browser"),
        (("0:browser", "1:terminal", "3:mplayer"), "3:mplayer", True, "1:terminal"),
        # number + letter
        (("0:browser", "1:terminal", "a:mplayer"), "0:browser", True, "a:mplayer"),
        (("0:browser", "1:terminal", "a:mplayer"), "1:terminal", True, "0:browser"),
        (("0:browser", "1:terminal", "a:mplayer"), "a:mplayer", True, "1:terminal"),
    ]

    for wpids, curwpid, cyclic, expect in feed:
        nextwpid = navigating.prev_wpid(curwpid, wpids, cyclic)
        assert nextwpid == expect
