import pytest
import trio
from palantir.i3ipc import API, Connection
from palantir.i3ipc.protocol import MessageType, ReplyType


def _i3_unreachable():
    import shlex
    import subprocess

    try:
        subprocess.run(
            shlex.split("command -v i3 || command -v sway"), shell=True, check=True
        )
    except subprocess.CalledProcessError:
        return True
    else:
        return False


when_i3_reachable = pytest.mark.skipif(_i3_unreachable(), reason="i3 was not reachable")


@pytest.fixture(scope="function")
async def conn():
    return await Connection.from_local()


@when_i3_reachable
async def test_get_version(conn: Connection):
    async with conn:
        reply = await conn.round_trip(MessageType.GET_VERSION, b"")
        assert "major" in reply
        assert "human_readable" in reply


@when_i3_reachable
async def test_send_sequence_commands(conn: Connection):
    async with conn:
        await conn._send_message(MessageType.GET_VERSION, b"")
        await conn._send_message(MessageType.GET_WORKSPACES, b"")
        await conn._send_message(MessageType.GET_OUTPUTS, b"")

        reply_type, reply = await conn.recv_reply()
        assert reply_type == ReplyType.VERSION
        assert "major" in reply

        reply_type, reply = await conn.recv_reply()
        assert reply_type == ReplyType.WORKSPACES
        for item in reply:
            assert "num" in item
            assert "name" in item

        reply_type, reply = await conn.recv_reply()
        assert reply_type == ReplyType.OUTPUTS
        for item in reply:
            assert "name" in item
            assert "active" in item
            assert "primary" in item


@when_i3_reachable
async def test_recv_timeout(conn: Connection):
    with pytest.raises(trio.TooSlowError):
        async with conn:
            with trio.fail_after(1):
                await conn.recv_reply()


@when_i3_reachable
async def test_basic_apis(conn: Connection):
    async with conn:
        api = API(conn)

        await api.get_version()
        await api.get_workspaces()
        await api.get_tree()
        await api.get_outputs()
        await api.get_marks()
        await api.get_binding_modes()
        await api.get_binding_state()
