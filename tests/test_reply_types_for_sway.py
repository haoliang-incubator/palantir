from palantir.i3ipc import reply_types


def test_workspaces(data_loader):
    data = data_loader("get_workspaces.reply.sway")
    cls = reply_types.Workspace

    for item in data:
        instance = cls.from_dict(item)
        assert isinstance(instance, cls)


def test_outputs(data_loader):
    data = data_loader("get_outputs.reply.sway")
    cls = reply_types.Output

    for item in data:
        instance = cls.from_dict(item)
        assert isinstance(instance, cls)


def test_tree(data_loader):
    data = data_loader("get_tree.reply.sway")
    cls = reply_types.Tree

    instance = cls.from_dict(data)
    assert isinstance(instance, cls)


def test_version(data_loader):
    data = data_loader("get_version.reply.sway")
    cls = reply_types.Version

    instance = cls.from_dict(data)
    assert isinstance(instance, cls)
    assert instance.variant == "sway"


def test_binding_state(data_loader):
    data = data_loader("get_binding_state.reply.sway")
    cls = reply_types.BindingState

    instance = cls.from_dict(data)
    assert isinstance(instance, cls)


def test_command(data_loader):
    data = data_loader("command.reply.sway")

    cls = reply_types.Command

    instance = cls.from_dict(data[0])
    assert isinstance(instance, cls)
    assert instance.success

    instance = cls.from_dict(data[1])
    assert isinstance(instance, cls)
    assert not instance.success


def test_subscribe(data_loader):
    data = data_loader("subscribe.reply.sway")

    cls = reply_types.Subscribe

    instance = cls.from_dict(data)
    assert isinstance(instance, cls)


def test_marks(data_loader):
    # todo implement
    pass


def test_bar_config(data_loader):
    # todo implement
    pass


def test_binding_modes(data_loader):
    # todo implement
    pass


def test_config(data_loader):
    # todo implement
    pass
