import datetime
import random

import pytest
import trio
from palantir.i3ipc.tricycle import RWLock


@pytest.mark.skip(reason="no need")
async def test_rwlock():
    async def reader(id, rwlock: RWLock):
        async with rwlock.read_locked():
            seconds = random.randrange(0, 3)
            print(
                f"#{id} holding a READ lock for {seconds}s ({datetime.datetime.now()})"
            )
            await trio.sleep(seconds)

    async def writer(id, rwlock: RWLock):
        async with rwlock.write_locked():
            seconds = random.randrange(0, 3)
            print(
                f"#{id} holding a WRITE lock for {seconds}s ({datetime.datetime.now()})"
            )
            await trio.sleep(seconds)

    rwlock = RWLock()

    async with trio.open_nursery() as nursery:
        for idx in range(5):
            nursery.start_soon(reader, idx, rwlock)

        for idx in range(6, 6 + 2):
            nursery.start_soon(writer, idx, rwlock)

        for idx in range(8, 8 + 5):
            nursery.start_soon(reader, idx, rwlock)

        for idx in range(13, 13 + 2):
            nursery.start_soon(writer, idx, rwlock)

    # TODO@haoliang assert


if __name__ == "__main__":
    trio.run(test_rwlock)
