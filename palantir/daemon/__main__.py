if __name__ == "__main__":

    import logging

    logging.basicConfig(
        level="DEBUG",
        style="{",
        format="{message}",
    )

    import trio
    from palantir.daemon.main import main

    trio.run(main)
