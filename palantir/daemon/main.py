import datetime
import json
import logging
import os
import pathlib
import shlex
import time
from argparse import ArgumentParser
from contextlib import AsyncExitStack, contextmanager
from signal import SIGUSR1, SIGUSR2
from traceback import TracebackException

import palantir.cli.main as cli
import trio
from palantir import i3ipc, rice
from pympler import muppy, summary
from pympler.tracker import SummaryTracker
from trio import BrokenResourceError, MultiError
from trio.lowlevel import FdStream

try:
    _userdir = pathlib.Path(os.environ["XDG_RUNTIME_DIR"])
except KeyError:
    _userdir = pathlib.Path("/run/user/{}".format(os.getuid()))

assert _userdir.is_dir()
PIPEFILE = _userdir.joinpath("palantir.fifo")


class Retryable(BrokenResourceError):
    ...


@contextmanager
def verbose_error():
    try:
        yield
    except Exception as exc:  # pylint: disable=broad-except
        traceback = TracebackException.from_exception(
            exc, lookup_lines=True, capture_locals=True
        )
        frames = traceback.format()

        for frame in frames:
            logging.error(frame)

        raise exc


async def aretry(afn, times=3, retryable_exc=(Retryable,), interval=0.1):
    errors = []
    remain = times
    while remain > 0:
        try:
            return await afn()
        except retryable_exc as e:
            errors.append(e)
            remain -= 1
            await trio.sleep(interval)

    raise MultiError(errors)


async def _read_command(pipefile: str):
    """
    # TODO@haoliang support `;` as an end character?
    """

    fd = os.open(pipefile, os.O_RDWR)

    stream: FdStream
    async with FdStream(fd) as stream:
        bufsize = 1 << 8
        recv = stream.receive_some
        buf = bytearray()

        while True:
            buf.extend(await recv(bufsize))

            while True:
                break_at = buf.find(b"\n")
                if break_at < 0:
                    break

                yield bytes(buf[: break_at + 1])
                del buf[: break_at + 1]


async def _run_command(api: i3ipc.API, command: bytes):
    _args = shlex.split(command.decode())

    start = time.time()

    # pylint: disable=bare-except
    try:
        await cli.main_with_args(api, cli.parse_args(_args))
    except trio.BrokenResourceError:
        raise
    except:  # noqa: E722
        logging.exception("")
    finally:
        end = time.time()
        logging.info("elapsed %s ms", round((end - start) * 1000, 2))


async def _listen_and_run_command():
    try:
        os.mkfifo(PIPEFILE, mode=0o600)
    except FileExistsError:
        pass

    conn = await i3ipc.Connection.from_local()
    async with conn:

        api = i3ipc.API(conn)

        async for command in _read_command(PIPEFILE):
            if command == b"\n":
                continue

            with trio.move_on_after(5) as cancel:
                await _run_command(api, command)

            if cancel.cancelled_caught:
                logging.error("cancelled slow command: %s", command)


async def _listen_and_iconize_workspace_name():
    @contextmanager
    def _ignore_known_sysexit():
        def _unignored_args(e: SystemExit):
            for text in e.args:

                # Old workspace "2:2" not found
                if text.startswith("Old workspace") and text.endswith("not found"):
                    logging.error(text)
                    continue

                yield text

        try:
            yield
        except SystemExit as e:
            unignored = tuple(_unignored_args(e))
            if unignored:
                raise SystemExit(*unignored)

    window_from_dict = i3ipc.event_types.Window.from_dict
    NEW = i3ipc.event_types.WindowChange.NEW
    CLOSE = i3ipc.event_types.WindowChange.CLOSE
    MOVE = i3ipc.event_types.WindowChange.MOVE
    iconize_current_workspace = rice.commands.iconize_workspace
    iconize_all_workspaces = rice.commands.iconize_all_workspaces

    stack: AsyncExitStack
    async with AsyncExitStack() as stack:
        subscriber_conn = await i3ipc.Connection.from_local()
        await stack.enter_async_context(subscriber_conn)

        command_conn = await i3ipc.Connection.from_local()
        await stack.enter_async_context(command_conn)

        api = i3ipc.API(command_conn)

        _reply = await subscriber_conn.round_trip(
            i3ipc.protocol.MessageType.SUBSCRIBE,
            json.dumps([i3ipc.protocol.EventType.WINDOW.to_string()]).encode(),
        )
        reply = i3ipc.reply_types.Subscribe.from_dict(_reply)
        if not reply.success:
            raise SystemExit("failed to subscribe events from i3wm")

        while True:
            try:
                _, _reply = await subscriber_conn.recv_reply()
            except BrokenResourceError as e:
                raise Retryable from e

            reply = window_from_dict(_reply)

            with _ignore_known_sysexit():
                if reply.change == NEW:
                    await iconize_current_workspace(api)
                elif reply.change == CLOSE:
                    # could close window in un-focused workspace
                    await iconize_all_workspaces(api)
                elif reply.change == MOVE:
                    await iconize_all_workspaces(api)
                else:
                    continue


async def _mem_stat():
    tracker = SummaryTracker()

    dumproot = trio.Path("/").joinpath(
        "tmp",
        "{}-palantir-mem-stat".format(os.getuid()),
        "pid-{}".format(os.getpid()),
    )
    await dumproot.mkdir(parents=True, exist_ok=True)

    async def dump_summary():
        dumpfile = dumproot.joinpath(
            "{}-{}".format(
                "summary",
                datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
            )
        )
        logging.info("dumpping mem summary into %s", dumpfile)

        allobjs = muppy.get_objects()

        async with await dumpfile.open("wb") as fp:
            lines = summary.format_(summary.summarize(allobjs), limit=4 << 10)
            for line in lines:
                await fp.write(f"{line}\n".encode())

    async def dump_summary_diff():
        dumpfile = dumproot.joinpath(
            "{}-{}".format(
                "diff",
                datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
            )
        )
        logging.info("dumpping mem diff into %s", dumpfile)

        async with await dumpfile.open("wb") as fp:
            lines = summary.format_(tracker.diff(), limit=1 << 10)
            for line in lines:
                await fp.write(f"{line}\n".encode())

    with trio.open_signal_receiver(SIGUSR1, SIGUSR2) as receiver:
        async for sig in receiver:
            if sig == SIGUSR1:
                await dump_summary()
            elif sig == SIGUSR2:
                await dump_summary_diff()


def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        "--iconize",
        action="store_true",
        help="enable iconize workspace name automacaly",
    )

    return parser.parse_args()


async def main():
    args = parse_args()

    async def _auto_retry(fn):
        with verbose_error():
            await aretry(fn, retryable_exc=(ConnectionRefusedError, Retryable))

    async with trio.open_nursery() as nursery:
        # TODO@haoliang isolate _listen_and_run_command and _listen_and_iconize_workspace_name
        # TODO@haoliang reuse command_conn between _listen_and_run_command and _listen_and_iconize_workspace_name
        # TODO@haoliang reduce number of connections: standaone task deal with conn recv_reply, and dispatch events

        nursery.start_soon(_mem_stat)

        nursery.start_soon(_auto_retry, _listen_and_run_command)

        if args.iconize:
            nursery.start_soon(_auto_retry, _listen_and_iconize_workspace_name)
