import logging
from itertools import chain

from palantir.i3ipc import API
from palantir.i3ipc.reply_types import Tree

from ..iconizing import Root, Workspace, wptrees_in_root_tree
from ._utils import build_command, interpret_replies, treat_as_sysexit

_log = logging.getLogger(__name__)


async def iconize_workspace(api: API, wpno=None):
    root = await api.get_tree()

    with treat_as_sysexit(KeyError):
        if wpno is None:
            wptree = _focused_workspace_tree(root)
        else:
            wptree = _workspace_tree(root, wpno)

    with treat_as_sysexit(ValueError):
        workspace = Workspace.from_workspace_tree(wptree, api._conn.variant)

    if not workspace.wpid_changed:
        _log.debug("workspace name has not changed")
        return

    command = build_command(
        "rename workspace {} to {}", workspace.wpid, workspace.iconized
    )
    _log.debug("iconize_workspace: %s", command)

    interpret_replies(await api.run_command(command))


async def iconize_all_workspaces(api: API):
    def _renames(root: Root):
        template = "rename workspace {} to {}"
        for wp in root.workspaces:
            if wp.wpid_changed:
                yield build_command(template, wp.wpid, wp.iconized)

    root = Root.from_root_tree(await api.get_tree(), api._conn.variant)

    command = ";".join(_renames(root))
    if not command:
        _log.debug("iconize_all_workspaces: no changes, skipped")
        return

    _log.debug("iconize_all_workspaces: %s", command)

    interpret_replies(await api.run_command(command))


def _focused_workspace_tree(root: Tree):
    # according to WORKSPACES reply, only one workspace can have the focus at the same time
    def has_focused_con(workspace: Tree):
        def find(root: Tree):
            if root.focused:
                return True

            # in case a floating window got focused
            for i in chain(root.nodes, root.floating_nodes):
                if find(i):
                    return True

            return False

        return find(workspace)

    for wptree in wptrees_in_root_tree(root):
        if has_focused_con(wptree):  # type: ignore
            return wptree

    raise KeyError


def _workspace_tree(root: Tree, wpno: str) -> Tree:
    prefix = f"{wpno}:"

    for wptree in wptrees_in_root_tree(root):
        if wptree.name.startswith(prefix):  # type: ignore
            return wptree

    raise KeyError
