import contextlib


def interpret_replies(replies: list):
    def one_by_one():
        for reply in replies:
            try:
                success = reply.success
            except AttributeError:
                yield "i3 reply without bool(success)"
                continue

            if success:
                continue

            try:
                error = reply.error
            except AttributeError:
                yield "i3 reply neither success nor error"
            else:
                yield error

    error = SystemExit(*one_by_one())
    if error.args:
        raise error


def find_curwpid(wps):
    for w in wps:
        if w.focused:
            return w.name

    raise RuntimeError("can not find current workspace")


@contextlib.contextmanager
def treat_as_sysexit(*types):
    try:
        yield
    except types as e:
        raise RuntimeError(repr(e)) from e


def build_command(fmt: str, *args: str):
    def _quoted():
        for a in args:
            if not isinstance(a, str):
                a = str(a)

            yield '"{}"'.format(a.replace('"', '\\"'))

    return fmt.format(*_quoted())
