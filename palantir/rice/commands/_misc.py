# pylint: disable=raise-missing-from

import logging

from palantir.i3ipc import API

from .. import naming, navigating
from ._utils import build_command, find_curwpid, interpret_replies

_log = logging.getLogger(__name__)


async def jump_to(api: API, wpno):
    wps = await api.get_workspaces()
    wpids = [w.name for w in wps]

    try:
        wpid = navigating.wpno_to_wpid(wpno, wpids)
    except KeyError:
        wpid = naming.allocate_wpid(wpno, "", wpids)

    command = build_command("workspace {}", wpid)
    _log.debug("jump_to: %s", command)

    interpret_replies(await api.run_command(command))


async def move_to(api: API, wpno):
    wps = await api.get_workspaces()
    wpids = [w.name for w in wps]

    try:
        wpid = navigating.wpno_to_wpid(wpno, wpids)
    except KeyError:
        wpid = naming.allocate_wpid(wpno, "", wpids)

    command = build_command("move container to workspace {}", wpid)
    _log.debug("move_to: %s", command)

    interpret_replies(await api.run_command(command))


async def rename(api: API, new):
    wps = await api.get_workspaces()
    wpids = [w.name for w in wps]
    curwpid = find_curwpid(wps)
    dest_wpid = naming.allocate_wpid(new, curwpid, wpids)

    command = build_command("rename workspace {} to {}", curwpid, dest_wpid)
    _log.debug("rename: %s", command)

    interpret_replies(await api.run_command(command))


async def navigate(api: API, direction, acyclic: bool):
    wps = await api.get_workspaces()
    wpids = [w.name for w in wps]
    curwpid = find_curwpid(wps)

    try:
        if direction == "next":
            dst_wpid = navigating.next_wpid(curwpid, wpids, not acyclic)
        elif direction == "prev":
            dst_wpid = navigating.prev_wpid(curwpid, wpids, not acyclic)
        else:
            raise SystemExit("unknown direction")
    except navigating.Unswitchable as e:
        raise SystemExit(repr(e))

    command = build_command("workspace {}", dst_wpid)
    _log.debug("navigate: %s", command)

    interpret_replies(await api.run_command(command))
