from ._iconize import iconize_all_workspaces, iconize_workspace
from ._misc import jump_to, move_to, navigate, rename
