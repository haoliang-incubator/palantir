"""
wpno: char([0-9a-z])
codepoint: 0=48, 9=57, a=97, z=122
order: 0-9 -> a-z -> 0-9
"""

# pylint: disable=raise-missing-from

import bisect
import logging
from typing import Iterable

from . import naming

_log = logging.getLogger(__name__)


class Unswitchable(RuntimeError):
    pass


def _switchable_wp_pairs(wpids: Iterable[str]) -> Iterable[tuple[int, str]]:
    for wpid in wpids:
        try:
            wpno = naming.extract_wpno(wpid)
        except ValueError:
            continue
        else:
            yield ord(wpno), wpid


def next_wpid(curwpid: str, wpids: Iterable[str], cyclic: bool):
    curwpno = naming.extract_wpno(curwpid)
    curwpord = ord(curwpno)
    pairs = dict(_switchable_wp_pairs(wpids))
    wpords = sorted(pairs.keys())

    _log.debug("navigator.next; curwpno: %s, pairs: %s", curwpno, pairs)

    try:
        dest_wpord = wpords[bisect.bisect_right(wpords, curwpord)]
    except IndexError:
        if not cyclic:
            raise Unswitchable("cannot find next workspace name")

        dest_wpord = wpords[0]

    return pairs[dest_wpord]


def prev_wpid(curwpid: str, wpids: Iterable[str], cyclic: bool):
    curwpno = naming.extract_wpno(curwpid)
    curwpord = ord(curwpno)
    pairs = dict(_switchable_wp_pairs(wpids))
    wpords = sorted(pairs.keys())

    _log.debug("navigator.prev; curwpno: %s, pairs: %s", curwpno, pairs)

    try:
        dest_wpord = wpords[bisect.bisect_left(wpords, curwpord) - 1]
    except IndexError:
        if not cyclic:
            raise Unswitchable("cannot find previous workspace name")

        dest_wpord = wpords[0]

    return pairs[dest_wpord]


def wpno_to_wpid(wpno: int, wpids: Iterable[str]):
    prefix = f"{wpno}:"

    for wpid in wpids:
        if wpid.startswith(prefix):
            return wpid

    raise KeyError(prefix)
