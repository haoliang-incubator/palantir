"""
naming of workspace

form 1: wpid = no
* no: [0-9] | [a-z]

form 2: wpid = no:name
* no: [0-9] | [a-z]
* name: .+
"""

# pylint: disable=raise-missing-from

import string
from typing import Iterable

Char = str

DELIMITER = ":"
LETTER_POOL = set(string.ascii_lowercase)
DIGIT_POOL = set(string.digits)
POOL = LETTER_POOL | DIGIT_POOL


def extract_wpno(wpid: str) -> Char:
    """
    :raise: ValueError
    """

    delat = wpid.find(DELIMITER)

    if delat < 0:
        # no delimiter, wpid should be a number (i3wm's default naming convention)
        try:
            wpno = str(int(wpid))
        except ValueError:
            raise ValueError("invalid wpid: not a number")
        if wpno != wpid:
            raise ValueError("invalid wpid: leading zero?")
    else:
        wpno = wpid[:delat]

    # wpid could be a number or letter
    if wpno not in POOL:
        raise ValueError("invalid wpno: unknown char")

    return wpno


def _allocate_wpno(given_name: str, allocated_wpnoes: set[Char]) -> str:

    # try to allocate from the given
    for char in given_name:
        if char not in POOL:
            continue
        if char not in allocated_wpnoes:
            return char

    # try to allocate from pool
    available = POOL - allocated_wpnoes

    try:
        return available.pop()
    except KeyError:
        raise RuntimeError("workspace id was exhaused")


def _allocated_wpnoes(wpids: Iterable[str]) -> Iterable[Char]:
    for wpid in wpids:
        try:
            wpno = extract_wpno(wpid)
        except ValueError:
            continue
        else:
            yield wpno


def allocate_wpid(given_name: str, curwpid: str, wpids: Iterable[str]):
    """
    :return: no:name

    todo:
    * (0, 1:terminal); mv 1:terminal 0
    """
    allocated_wpnoes = set(_allocated_wpnoes(wpids))

    try:
        givenwpno = extract_wpno(given_name)
    except ValueError:
        givenwpno = "!"

    try:
        curwpno = extract_wpno(curwpid)
    except ValueError:
        curwpno = "!"

    if givenwpno == "!":
        if curwpno == "!":
            # random one
            dest_wpid = "{}:{}".format(_allocate_wpno(given_name, allocated_wpnoes), given_name)
        else:
            dest_wpid = "{}:{}".format(curwpno, given_name)
    else:
        if curwpno == "!":
            # avoid duplicate
            if givenwpno in allocated_wpnoes:
                raise RuntimeError("wpno exists")
            dest_wpid = given_name
        else:
            # avoid duplicate except me
            if givenwpno == curwpno:
                # given_name only contains wpno, no change
                if givenwpno == given_name:
                    dest_wpid = curwpid
                else:
                    dest_wpid = given_name
            else:
                if givenwpno in allocated_wpnoes:
                    raise RuntimeError("wpno exists")
                dest_wpid = given_name

    return dest_wpid
