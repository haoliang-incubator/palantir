"""
client properties in X11
* https://www.x.org/releases/X11R7.6/doc/xorg-docs/specs/ICCCM/icccm.html#client_properties

wayland ?


* programID: wm_class, app_id
* workspace: {name, programs: list[programID]}
    * programs order?
* icon (program_icon)

"""

import logging
from collections import Counter
from typing import Iterable

import attr
from palantir.i3ipc import Variant
from palantir.i3ipc.reply_types import Tree, TreeType

from . import naming

_log = logging.getLogger(__name__)

ProgID = str
Icon = str
WPNO = str
WPID = str

# icons come from nerd font
PROG_ICON: dict[ProgID, Icon] = {
    # default!
    "default": chr(0xF2D0),
    # browser
    "firefox": chr(0xE745),
    "chromium": chr(0xFA9E),
    "vivaldi-stable": chr(0xFA9E),
    # terminal
    "urxvt": chr(0xE7A2),
    "alacritty": chr(0xE7A2),
    # video player
    "mplayer": chr(0xF01D),
    "mpv": chr(0xF01D),
    # misc
    "sxiv": chr(0xF03E),
    "scrcpy": chr(0xE70E),
    "deluge": chr(0xF019),
}


def programs_in_wptree(wptree: Tree, variant: Variant) -> Iterable[ProgID]:
    def _class(con: Tree):
        assert con.window_properties

        return con.window_properties.klass.lower()

    def _appid(con: Tree):
        assert con.app_id

        return con.app_id.lower()

    def _leaves(tree: Tree) -> Iterable[Tree]:
        if not tree.nodes:
            if tree.type == TreeType.CON:
                yield tree
            return

        for i in tree.nodes:
            yield from _leaves(i)

    assert wptree.type == TreeType.WORKSPACE

    if variant == Variant.I3:
        finder = _class
    elif variant == Variant.SWAY:
        finder = _appid
    else:
        raise ValueError(f"invalid i3 variant: {variant}")

    for con in _leaves(wptree):
        yield finder(con)


def wptrees_in_root_tree(root: Tree) -> Iterable[Tree]:
    def _workspaces(tree: Tree):

        if tree.type == TreeType.WORKSPACE:
            if not tree.is_scratchpad:
                yield tree
            return

        for i in tree.nodes:
            yield from _workspaces(i)

    assert root.type == TreeType.ROOT

    return _workspaces(root)


@attr.s
class Workspace:
    wpno: WPNO = attr.ib()
    wpid: WPID = attr.ib()
    programs: list[ProgID] = attr.ib()

    iconized: WPID = attr.ib(init=False)

    def __attrs_post_init__(self):
        self.iconized = self._iconize()

    @property
    def wpid_changed(self) -> bool:
        return self.wpid != self.iconized

    @classmethod
    def from_workspace_tree(cls, wptree: Tree, variant: Variant):
        """
        :raise: ValueError wpid is not legal
        """
        wpid = wptree.name
        assert wpid

        wpno = naming.extract_wpno(wpid)

        programs = list(programs_in_wptree(wptree, variant))

        return cls(wpno, wpid, programs)

    def _iconize(self) -> str:
        def icon(progid: ProgID):
            try:
                return PROG_ICON[progid]
            except KeyError:
                return PROG_ICON["default"]

        icons = Counter(icon(progid) for progid in self.programs)
        name = "".join(f"{icon}{count}" for icon, count in icons.items())

        return "{}:{}".format(self.wpno, name or "_blank")


@attr.s
class Root:
    workspaces: list[Workspace] = attr.ib()

    @classmethod
    def from_root_tree(cls, root: Tree, variant: Variant):
        def _workspaces():
            for wptree in wptrees_in_root_tree(root):
                try:
                    yield Workspace.from_workspace_tree(wptree, variant)
                except ValueError as e:
                    _log.error(
                        "skipped workspace %s, due to invalid wpid",
                        wptree.name,
                        exc_info=e,
                    )
                    continue

        return cls(list(_workspaces()))
