import argparse
import logging

import trio


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("wpno")
    return parser.parse_args()


async def main():
    from palantir import i3ipc
    from palantir.rice import commands

    args = parse_args()

    conn = await i3ipc.Connection.from_local()

    async with conn:
        api = i3ipc.API(conn)

        await commands.iconize_workspace(api, args.wpno)


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{asctime} {message}",
    )

    trio.run(main)
