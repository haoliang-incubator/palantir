import argparse
import logging
from pathlib import Path


class Facts:
    root = Path(__file__).resolve().parent.parent
    unit = "palantir.service"
    unit_target = Path.home().joinpath(".config", "systemd", "user", unit)
    unit_template = r"""
[Unit]
Description=palantir daemon

[Service]
Type=simple
Environment="PYTHONPATH={root}"
ExecStart={root}/venv/bin/python -m palantir.daemon {options}

[Install]
WantedBy=multi-user.target
"""
    guide = """
some guids of using palantir systemd service:
* dont forget `--user` option when using systemctl, journalctl
* user should never `systemctl --user enable palantir.service`
* debug log: `journalctl --user --unit palantir.service --follow`
"""


def parse_args():
    parser = argparse.ArgumentParser(
        epilog=Facts.guide, formatter_class=argparse.RawTextHelpFormatter
    )
    sub = parser.add_subparsers(dest="op")
    install = sub.add_parser("install")
    install.add_argument(
        "-f", "--force", action="store_true", help="overwrite existed service file"
    )
    install.add_argument(
        "--iconize", action="store_true", help="enables iconize workspace name"
    )

    return parser.parse_args()


def _install_service(iconize: bool, force: bool):
    target = Facts.unit_target

    if force:
        target.unlink(missing_ok=True)

    if target.exists():
        logging.info("service already exists in %s", target)
        return

    options = []

    if iconize:
        options.append("--iconize")

    content = Facts.unit_template.format(root=Facts.root, options=" ".join(options))

    with target.open("wb") as fp:
        logging.info(
            "service has been installed in %s, you need manually restart it.", target
        )
        fp.write(content.encode())


def main():
    args = parse_args()

    if args.op == "install":
        _install_service(args.iconize, args.force)
    else:
        raise SystemExit(f"unknown op: %s {args.op}")


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{asctime} {message}",
    )

    main()
