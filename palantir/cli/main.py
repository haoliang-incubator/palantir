# pylint: disable=raise-missing-from

import argparse

from palantir import i3ipc
from palantir.rice import commands


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", action="store_true")
    subpars = parser.add_subparsers(dest="op", required=True)

    jumpto = subpars.add_parser("jumpto", help="jump to another workspace")
    jumpto.add_argument("wpno", type=str, help="workspace number")

    moveto = subpars.add_parser(
        "moveto", help="move current container to specific workspace"
    )
    moveto.add_argument("wpno", type=str, help="workspace number")

    rename = subpars.add_parser("rename", help="rename current workspace")
    rename.add_argument("new", type=str, help="new name")

    iconize = subpars.add_parser("iconize", help="iconize current workspace name")
    iconize.add_argument(
        "wpno", type=str, nargs="?", help="workspace number", default=None
    )

    nav = subpars.add_parser("navigate", help="navigate among workspaces")
    nav.add_argument("direction", choices=("prev", "next"))
    nav.add_argument("--acyclic", action="store_true")

    return parser.parse_args(args)


async def main_with_args(api: i3ipc.API, args):

    try:

        if args.op == "jumpto":
            await commands.jump_to(api, args.wpno)
        elif args.op == "moveto":
            await commands.move_to(api, args.wpno)
        elif args.op == "rename":
            await commands.rename(api, args.new)
        elif args.op == "navigate":
            await commands.navigate(api, args.direction, args.acyclic)
        elif args.op == "iconize":
            await commands.iconize_workspace(api, args.wpno)
        else:
            raise SystemExit("unsupported operation: {}".format(args.op))

    except RuntimeError as e:
        raise SystemExit(repr(e))


async def main():
    args = parse_args()

    async with await i3ipc.Connection.from_local() as conn:
        await main_with_args(i3ipc.API(conn), args)
