if __name__ == "__main__":

    import logging

    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{asctime} {message}",
    )

    import trio
    from palantir.cli.main import main

    trio.run(main)
