# pylint: disable=too-many-instance-attributes

from enum import Enum
from typing import List, Optional

import attr
from palantir.i3ipc._attrs import FromDictMixin, attrs_or_none


class _InputType(Enum):
    TOUCHPAD = "touchpad"
    KEYBOARD = "keyboard"
    SWITCH = "switch"


def _enabled_conv(val: str):
    if val is None:
        return None
    if val == "enabled":
        return True
    if val == "disabled":
        return False
    raise ValueError("val is not bool-able")


@attr.s
class _Libinput(FromDictMixin):
    send_events: bool = attr.ib(converter=_enabled_conv)


@attr.s
class _LibinputTouchpad(_Libinput):

    tap: bool = attr.ib(converter=_enabled_conv)
    # todo enum(lmr, lrm)
    tap_button_map: str = attr.ib()
    tap_drag: bool = attr.ib(converter=_enabled_conv)
    tap_drag_lock: bool = attr.ib(converter=_enabled_conv)
    accel_speed: float = attr.ib()
    # todo enum(none, flat, adaptive)
    accel_profile: str = attr.ib()
    natural_scroll: bool = attr.ib(converter=_enabled_conv)
    left_handed: bool = attr.ib(converter=_enabled_conv)
    # todo enum(none, two_finger, edge, on_button_down)
    scroll_method: str = attr.ib()
    # disable-while-typing
    dwt: bool = attr.ib(converter=_enabled_conv)

    # todo enum(none, button_areas, clickfinger)
    click_method: Optional[str] = attr.ib(default=None)
    # todo enum(none, button_areas, clickfinger)
    middle_emulation: Optional[bool] = attr.ib(
        default=None, converter=attrs_or_none(_enabled_conv)  # type: ignore
    )
    scroll_button: Optional[int] = attr.ib(default=None)
    calibration_matrix: Optional[List[float]] = attr.ib(default=None)


@attr.s
class Input(FromDictMixin):
    variants: List["Input"] = []

    identifier: str = attr.ib()
    name: str = attr.ib()
    vendor: int = attr.ib()
    product: int = attr.ib()

    def __init_subclass__(cls):
        super().__init_subclass__()
        Input.variants.append(cls)


@attr.s
class Switch(Input):
    type = _InputType.SWITCH
    libinput: _Libinput = attr.ib(converter=_Libinput.from_dict)  # type: ignore


@attr.s
class Keyboard(Input):
    type = _InputType.KEYBOARD
    xkb_active_layout_name: str = attr.ib()
    xkb_active_layout_index: int = attr.ib()
    xkb_layout_names: List[str] = attr.ib()
    libinput: _Libinput = attr.ib(converter=_Libinput.from_dict)  # type: ignore


@attr.s
class Touchpad(Input):
    type = _InputType.TOUCHPAD
    libinput: _LibinputTouchpad = attr.ib(converter=_LibinputTouchpad.from_dict)  # type: ignore


def input_from_dict(data: dict):
    try:
        input_type = data["type"]
    except KeyError as e:
        raise ValueError("given data contains no valid input") from e

    creators = {input_.type.value: input_ for input_ in Input.variants}  # type: ignore

    try:
        cls = creators[input_type]
    except KeyError as e:
        raise ValueError("unsupported input type") from e

    return cls.from_dict(data)
