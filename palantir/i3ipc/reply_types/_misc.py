from typing import Optional

import attr
from palantir.i3ipc._attrs import FromDictMixin


@attr.s
class Command(FromDictMixin):
    success: bool = attr.ib()
    parse_error: Optional[bool] = attr.ib(default=None)
    error: Optional[str] = attr.ib(default=None)


@attr.s
class Version(FromDictMixin):
    major: int = attr.ib()
    minor: int = attr.ib()
    patch: int = attr.ib()
    human_readable: str = attr.ib()
    loaded_config_file_name: str = attr.ib()
    # sway specific
    variant: Optional[str] = attr.ib(default=None)


@attr.s
class BindingState(FromDictMixin):
    name: str = attr.ib()


@attr.s
class Subscribe(FromDictMixin):
    success: bool = attr.ib()
