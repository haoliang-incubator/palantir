# pylint: disable=too-many-instance-attributes

from enum import Enum
from typing import List, Optional

import attr
from palantir.i3ipc._attrs import FromDictMixin, attrs_or_none

SCRATCHPAD_WORKSPACE_NAME = "__i3_scratch"


class _WindowType(Enum):
    UNKNOWN = "unknown"  # not list in official document
    UNDEFINED = None
    NORMAL = "normal"
    DIALOG = "dialog"
    UTILITY = "utility"
    TOOLBAR = "toolbar"
    SPLASH = "splash"
    MENU = "menu"
    DROPDOWN_MENU = "dropdown_menu"
    POPUP_MENU = "popup_menu"
    TOOLTIP = "tooltip"
    NOTIFICATION = "notification"


class _FullscreenMode(Enum):
    NO_FULLSCREEN = 0
    FULLSCREENED_ON_OUTPUT = 1
    FULLSCREENED_GLOBALLY = 2


class _TreeBorder(Enum):
    NORMAL = "normal"
    NONE = "none"
    PIXEL = "pixel"


class _TreeLayout(Enum):
    SPLITH = "splith"
    SPLITV = "splitv"
    STACKED = "stacked"
    TABBED = "tabbed"
    # i3 specific
    DOCKAREA = "dockarea"
    OUTPUT = "output"
    # occures in sway
    NONE = "none"


class TreeType(Enum):
    ROOT = "root"
    OUTPUT = "output"
    CON = "con"
    FLOATING_CON = "floating_con"
    WORKSPACE = "workspace"
    # i3 specific
    DOCKAREA = "dockarea"


@attr.s
class _WindowProperties(FromDictMixin):
    klass: str = attr.ib(metadata={"key": "class"})
    instance: str = attr.ib()
    transient_for: str = attr.ib()
    # when window_type==DIALOG, title will be None
    title: Optional[str] = attr.ib(default=None)
    window_role: Optional[str] = attr.ib(default=None)


def _nodes_from_dict(data: dict):
    return [Tree.from_dict(item) for item in data]


@attr.s
class _Rect(FromDictMixin):
    x: int = attr.ib()
    y: int = attr.ib()
    width: int = attr.ib()
    height: int = attr.ib()


@attr.s
class Tree(FromDictMixin):
    id: int = attr.ib(converter=int)  # pylint: disable=invalid-name
    name: Optional[str] = attr.ib()
    type: TreeType = attr.ib(converter=TreeType)
    border: _TreeBorder = attr.ib(converter=_TreeBorder)
    current_border_width: int = attr.ib()
    layout: _TreeLayout = attr.ib(converter=_TreeLayout)
    # orientation, since i3 has marked it as deprecated
    # sway says percent null for root, scratchpad
    percent: Optional[float] = attr.ib(converter=attrs_or_none(float))  # type: ignore
    rect: _Rect = attr.ib(converter=_Rect.from_dict)  # type: ignore
    window_rect: _Rect = attr.ib(converter=_Rect.from_dict)  # type: ignore
    deco_rect: _Rect = attr.ib(converter=_Rect.from_dict)  # type: ignore
    geometry: _Rect = attr.ib(converter=_Rect.from_dict)  # type: ignore
    urgent: bool = attr.ib()
    # bool sticky, sway specific
    marks: List[str] = attr.ib()
    focused: bool = attr.ib()
    focus: List[int] = attr.ib()
    fullscreen_mode: _FullscreenMode = attr.ib(converter=_FullscreenMode)
    nodes: List["Tree"] = attr.ib(converter=_nodes_from_dict)
    floating_nodes: List["Tree"] = attr.ib(converter=_nodes_from_dict)

    window: int = attr.ib()
    window_properties: Optional[_WindowProperties] = attr.ib(default=None, converter=attrs_or_none(_WindowProperties.from_dict))  # type: ignore
    # in sway, window_type occured in window_properties
    window_type: _WindowType = attr.ib(
        default=None, converter=attrs_or_none(_WindowType)  # type: ignore
    )

    # str representation, sway specific, type=workspace
    # optional[str] app_id, sway specific, type=view
    app_id: Optional[str] = attr.ib(default=None)
    # int pid, sway specific, type=view
    # bool visible, sway specific, type=view
    # str shell, sway specific, type=view
    # bool inhibit_idle, sway specific
    # obj idle_inhibitors, sway specific

    @property
    def is_scratchpad(self):
        return self.name == SCRATCHPAD_WORKSPACE_NAME


@attr.s
class Output(FromDictMixin):
    # TODO@haoliang additional fields comes from sway

    name: str = attr.ib()
    active: bool = attr.ib()
    primary: bool = attr.ib()
    current_workspace: str = attr.ib()
    rect: _Rect = attr.ib(converter=_Rect.from_dict)  # type: ignore


@attr.s
class Workspace(FromDictMixin):
    num: int = attr.ib()
    name: str = attr.ib()
    visible: bool = attr.ib()
    focused: bool = attr.ib()
    urgent: bool = attr.ib()
    rect: _Rect = attr.ib(converter=_Rect.from_dict)  # type: ignore
    output: str = attr.ib()

    @property
    def is_scratchpad(self):
        return self.name == SCRATCHPAD_WORKSPACE_NAME
