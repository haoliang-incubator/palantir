import functools
import json
import logging
import time
from typing import Union

import trio

from . import protocol
from ._socketpath import Variant, find_socket_path
from .protocol import EventType, MessageType, ReplyType
from .tricycle import RWLock

_log = logging.getLogger(__name__)


def _conn_retry(times: int):
    def meth_wrapper(fn):
        @functools.wraps(fn)
        async def meth(self: "Connection", *args, **kwargs):
            chances = times
            while True:
                try:
                    return await fn(self, *args, **kwargs)
                except trio.BrokenResourceError:
                    if chances <= 0:
                        _log.info("exceeded max retry times")
                        raise

                    await self._reconnect()
                    chances -= 1

        return meth

    return meth_wrapper


class Connection:
    max_recv_bytes_once = 8 << 10

    def __init__(self, variant: Variant, sock: trio.SocketStream, connected_at: float):
        self._variant = variant
        self._sock = sock
        self._rwlock = RWLock()
        self._connected_at = connected_at

    @property
    def variant(self) -> Variant:
        return self._variant

    @_conn_retry(times=3)
    async def _send_message(self, mt: MessageType, payload: Union[bytes, bytearray]):

        header = protocol.Header(length=len(payload), type=mt)

        # pylint: disable=not-async-context-manager
        async with self._rwlock.read_locked():
            await self._sock.send_all(header.pack())
            await self._sock.send_all(payload)

        await trio.sleep(0)

    async def recv_reply(self) -> tuple[Union[ReplyType, EventType], dict]:
        """there is need to re-connect since remote peer was intent to close the connection
        :raise: trio.BrokenResourceError
        """

        raw_header = await self._recv_exactly(protocol.HeaderStruct.SIZE)
        header = protocol.Header.from_bytes(raw_header)
        _reply = await self._recv_exactly(header.length)

        reply = json.loads(_reply)

        return header.type, reply

    async def _recv_exactly(self, n: int) -> Union[bytes, bytearray]:
        """
        :raise: trio.BrokenResourceError
        """

        out = bytearray()
        receive_some = self._sock.receive_some
        max_once = self.max_recv_bytes_once
        need_n = n

        while need_n > 0:
            chunk = await receive_some(min(max_once, need_n))
            if chunk == b"":
                raise trio.BrokenResourceError("remote peer has closed the connection")

            need_n -= len(chunk)
            out.extend(chunk)

        await trio.sleep(0)

        assert len(out) == n

        return out

    async def round_trip(
        self, mt: MessageType, payload: Union[bytes, bytearray]
    ) -> dict:

        await self._send_message(mt, payload)
        _, reply = await self.recv_reply()
        # TODO@haoliang ensure return-type matches message-type

        return reply

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, traceback):
        await self.aclose()

    async def aclose(self):
        await self._sock.aclose()

    @classmethod
    async def from_local(cls):
        variant, path = await find_socket_path()
        sock = await trio.open_unix_socket(path)
        connected_at = time.time()

        _log.debug("created a new connection to %s", variant)

        return cls(variant, sock, connected_at)

    async def _reconnect(self):
        now = time.time()

        # pylint: disable=not-async-context-manager
        async with self._rwlock.write_locked():
            if now <= self._connected_at:
                _log.info("a new connection is available already")
                return

            variant, path = await find_socket_path()
            sock = await trio.open_unix_socket(path)
            connected_at = time.time()

            self._variant = variant
            self._sock = sock
            self._connected_at = connected_at

            _log.info("created a new connection to %s", variant)

        await trio.sleep(0)
