# pylint: disable=too-many-instance-attributes

import logging
from enum import Enum
from typing import Optional

import attr

from . import reply_types
from ._attrs import FromDictMixin, attrs_or_none

_log = logging.getLogger(__name__)


class WorkspaceChange(Enum):
    FOCUS = "focus"
    INIT = "init"
    EMPTY = "empty"
    URGENT = "urgent"
    RELOAD = "reload"
    RENAME = "rename"
    RESTORED = "restored"
    MOVE = "move"


@attr.s
class Workspace(FromDictMixin):
    change: WorkspaceChange = attr.ib(converter=WorkspaceChange)

    current: Optional[reply_types.Tree] = attr.ib(default=None, converter=attrs_or_none(reply_types.Tree.from_dict))  # type: ignore
    old: Optional[reply_types.Tree] = attr.ib(default=None, converter=attrs_or_none(reply_types.Tree.from_dict))  # type: ignore


class WindowChange(Enum):
    NEW = "new"
    CLOSE = "close"
    FOCUS = "focus"
    TITLE = "title"
    FULLSCREEN_MODE = "fullscreen_mode"
    MOVE = "move"
    FLOATING = "floating"
    URGENT = "urgent"
    MARK = "mark"


@attr.s
class Window(FromDictMixin):
    change: WindowChange = attr.ib(converter=WindowChange)
    container: reply_types.Tree = attr.ib(converter=reply_types.Tree.from_dict)  # type: ignore


class _ShutdownChange(Enum):
    RESTART = "restart"
    EXIT = "exit"


@attr.s
class Shutdown(FromDictMixin):
    change: _ShutdownChange = attr.ib(converter=_ShutdownChange)
