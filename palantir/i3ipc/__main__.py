import argparse
import itertools
import json
import logging
from traceback import TracebackException

import attr
import trio
from palantir import i3ipc
from palantir.i3ipc.reply_types import Tree, TreeType
from rich.logging import RichHandler


async def verbose(fn, *args, **kwargs):
    try:
        await fn(*args, **kwargs)
    except Exception as exc:  # pylint: disable=broad-except
        traceback = TracebackException.from_exception(
            exc, lookup_lines=True, capture_locals=True
        )
        frames = traceback.format()

        for frame in frames:
            logging.error(frame)


def _visual_tree(tree: i3ipc.reply_types.Tree, indent: int):

    name = str(tree.name)
    if len(name) >= 20:
        name = name[:20] + "..."

    print(
        '{} id={} name="{}" type={} layout={} {}'.format(
            " " * indent,
            tree.id,
            name,
            tree.type.name,
            tree.layout.name,
            "focused " if tree.focused else "",
        )
    )

    for node in itertools.chain(tree.nodes, tree.floating_nodes):
        _visual_tree(node, indent + 4)


async def debug_dockarea():
    def _visual_all_dockarea(root: i3ipc.reply_types.Tree):

        DOCKAREA = i3ipc.reply_types.TreeType.DOCKAREA
        chain = itertools.chain

        # pylint: disable=unused-argument
        def without_nodes(attribute: attr.Attribute, value):
            return attribute.name != "nodes"

        def _visual_dockarea(tree: i3ipc.reply_types.Tree, indent: int):
            if tree.type == DOCKAREA:
                print(
                    "{} {}".format(
                        " " * indent, attr.asdict(tree, filter=without_nodes)
                    )
                )

            for node in chain(tree.nodes, tree.floating_nodes):
                _visual_dockarea(node, indent + 2)

        _visual_dockarea(root, 0)

    conn = await i3ipc.Connection.from_local()
    async with conn:
        api = i3ipc.API(conn)
        tree = await api.get_tree()
        # _visual_tree(tree, 0)
        _visual_all_dockarea(tree)


async def debug_workspace_event():
    conn = await i3ipc.Connection.from_local()
    async with conn:
        _reply = await conn.round_trip(
            i3ipc.protocol.MessageType.SUBSCRIBE,
            json.dumps([i3ipc.protocol.EventType.WORKSPACE.to_string()]).encode(),
        )
        reply = i3ipc.reply_types.Subscribe.from_dict(_reply)
        assert reply.success

        while True:
            event_type, _reply = await conn.recv_reply()
            assert isinstance(event_type, i3ipc.protocol.EventType)

            reply = i3ipc.event_types.Workspace.from_dict(_reply)
            print(f"{reply.change=}")
            if reply.current:
                print("reply.current")
                _visual_tree(reply.current, 2)

            if reply.old:
                print("reply.old")
                _visual_tree(reply.old, 2)


async def goto_prev_workspace():
    conn = await i3ipc.Connection.from_local()
    async with conn:
        reply = await conn.round_trip(
            i3ipc.protocol.MessageType.COMMAND, b"workspace prev"
        )

    print(reply)


async def debug_container_tree():
    conn = await i3ipc.Connection.from_local()
    async with conn:
        api = i3ipc.API(conn)
        tree = await api.get_tree()
        _visual_tree(tree, 0)


async def debug_socketpath():
    path = await i3ipc.find_socket_path()
    print(path)


async def get_inputs():
    conn = await i3ipc.Connection.from_local()
    async with conn:
        api = i3ipc.API(conn)
        inputs = await api.get_inputs()

    for inp in inputs:
        print(inp)


async def debug_scratchpad():
    def _scratchpad_wptree(root: Tree):
        def find(node: Tree):
            if node.type == TreeType.WORKSPACE:
                if node.name == "__i3_scratch":
                    yield node
                return

            for subnode in itertools.chain(node.nodes, node.floating_nodes):
                yield from find(subnode)

        try:
            return next(find(root))
        except StopIteration:
            raise KeyError("no scratchpad found")

    def _floatcon(root: Tree):
        assert root.name == "__i3_scratch", f"not scratchpad tree {root.name}"

        def find(node: Tree):
            if node.type == TreeType.FLOATING_CON:
                yield node
                return

            for subnode in node.floating_nodes:
                yield from find(subnode)

        return find(root)

    conn = await i3ipc.Connection.from_local()
    async with conn:
        api = i3ipc.API(conn)
        root = await api.get_tree()
        scratchpad = _scratchpad_wptree(root)
        for con in _floatcon(scratchpad):
            con.rect = con.window_rect = con.deco_rect = con.geometry = None
            # con.nodes = con.floating_nodes = None
            print(con, "\n")


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "command",
        choices=tuple(
            getattr(command, "__name__")
            for command in (
                debug_container_tree,
                debug_dockarea,
                debug_workspace_event,
                goto_prev_workspace,
                debug_socketpath,
                get_inputs,
                debug_scratchpad,
            )
        ),
    )

    return parser.parse_args(args)


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )

    args = parse_args()
    try:
        command = locals()[args.command]
    except KeyError:
        parse_args(["-h"])
    else:
        trio.run(verbose, command)
