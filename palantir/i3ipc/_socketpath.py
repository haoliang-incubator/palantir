import contextlib
import logging
import os
import subprocess
from enum import Enum

import trio

_log = logging.getLogger(__name__)


class Variant(Enum):
    I3 = "i3"
    SWAY = "sway"


async def _via_env(var: str) -> trio.Path:
    try:
        _path = os.environ[var]
    except KeyError as e:
        _log.debug("ENV.%s not set", var)
        raise FileNotFoundError from e

    path = trio.Path(_path)

    if await path.is_socket():
        _log.debug("ENV.%s is a valid socket: %s", var, path)
        return path

    _log.debug("ENV.%s is not a valid socket", var)
    raise FileNotFoundError


async def _via_cmd(cmd: list):
    try:
        cp = await trio.run_process(
            cmd,
            capture_stdout=True,
            check=True,
        )
    except subprocess.CalledProcessError as e:
        _log.debug("CMD.%s did not provide a valid socket: %s", cmd, e)
        # pylint: disable=raise-missing-from
        raise FileNotFoundError

    _path = cp.stdout.decode().strip()
    if not _path:
        _log.debug("CMD.%s did not provide a valid socket: %s", cmd, _path)
        raise FileNotFoundError

    path = trio.Path(_path)

    if await path.is_socket():
        _log.debug("CMD.%s provides a valid socket: %s", cmd, path)
        return path

    _log.debug("CMD.%s did not provide a valid socket: %s", cmd, path)
    raise FileNotFoundError


async def _via_filesystem_for_i3(uid: int):
    # respect XDG_RUNTIME_DIR?
    # /run/user/$uid/i3/ipc-socket.$pid

    path = trio.Path(f"/run/user/{uid}/i3")

    try:
        files = await path.iterdir()
    except FileNotFoundError:
        _log.debug("XDG_RUNTIME_DIR.i3 did not exist")
        raise

    for file in files:
        if await file.is_dir():
            continue

        if file.name.startswith("ipc-socket."):
            sock = file
            break
    else:
        _log.debug("XDG_RUNTIME_DIR.i3 did not contain a valid socket")
        raise FileNotFoundError

    if await sock.is_socket():
        _log.debug("XDG_RUNTIME_DIR.i3 contains a valid socket: %s", sock)
        return sock

    _log.debug("XDG_RUNTIME_DIR.i3 did not contain a valid socket: %s", sock)
    raise FileNotFoundError


async def _via_filesystem_for_sway(uid: int):
    # respect XDG_RUNTIME_DIR?
    # /run/user/$uid/sway-ipc-$uid.$pid.sock

    path = trio.Path(f"/run/user/{uid}")

    for file in await path.iterdir():
        if await file.is_dir():
            continue

        if file.name.startswith("sway-ipc."):
            sock = file
            break
    else:
        _log.debug("XDG_RUNTIME_DIR.sway did not contain a valid socket")
        raise FileNotFoundError

    if await sock.is_socket():
        _log.debug("XDG_RUNTIME_DIR.sway contains a valid socket: %s", sock)
        return sock

    _log.debug("XDG_RUNTIME_DIR.sway did not contain a valid socket: %s", sock)
    raise FileNotFoundError


@contextlib.contextmanager
def _ignore(*types):
    try:
        yield
    except types:
        pass


async def find_socket_path() -> tuple[Variant, trio.Path]:
    # partially stolen from python-ipc

    # first try environment variables
    with _ignore(FileNotFoundError):
        return Variant.I3, await _via_env("I3SOCK")

    with _ignore(FileNotFoundError):
        return Variant.SWAY, await _via_env("SWAYSOCK")

    with _ignore(FileNotFoundError):
        return Variant.I3, await _via_cmd(("i3", "--get-socketpath"))

    with _ignore(FileNotFoundError):
        return Variant.SWAY, await _via_cmd(("sway", "--get-socketpath"))

    uid = os.getuid()

    with _ignore(FileNotFoundError):
        return Variant.I3, await _via_filesystem_for_i3(uid)

    with _ignore(FileNotFoundError):
        return Variant.SWAY, await _via_filesystem_for_sway(uid)

    raise FileNotFoundError("can not find out where i3 sock exists")
