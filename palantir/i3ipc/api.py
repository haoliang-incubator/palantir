from . import reply_types
from .connection import Connection
from .protocol import MessageType


class API:
    def __init__(self, conn: Connection):
        self._conn = conn

    async def get_workspaces(self) -> list[reply_types.Workspace]:
        reply = await self._conn.round_trip(MessageType.GET_WORKSPACES, b"")

        return [reply_types.Workspace.from_dict(item) for item in reply]

    async def run_command(self, command: str) -> list[reply_types.Command]:
        reply = await self._conn.round_trip(MessageType.COMMAND, command.encode())

        return [reply_types.Command.from_dict(item) for item in reply]

    async def get_outputs(self) -> list[reply_types.Output]:
        reply = await self._conn.round_trip(MessageType.GET_OUTPUTS, b"")

        return [reply_types.Output.from_dict(item) for item in reply]

    async def get_tree(self) -> reply_types.Tree:
        reply = await self._conn.round_trip(MessageType.GET_TREE, b"")

        return reply_types.Tree.from_dict(reply)

    async def get_marks(self) -> list[str]:
        reply = await self._conn.round_trip(MessageType.GET_MARKS, b"")

        return reply  # type: ignore

    async def get_version(self) -> reply_types.Version:
        reply = await self._conn.round_trip(MessageType.GET_VERSION, b"")

        return reply_types.Version.from_dict(reply)

    async def get_binding_modes(self) -> list[str]:
        reply = await self._conn.round_trip(MessageType.GET_BINDING_MODES, b"")

        return reply  # type: ignore

    async def get_binding_state(self) -> reply_types.BindingState:
        reply = await self._conn.round_trip(MessageType.GET_BINDING_STATE, b"")

        return reply_types.BindingState.from_dict(reply)

    async def get_inputs(self) -> list[reply_types.Input]:
        reply = await self._conn.round_trip(MessageType.GET_INPUTS, b"")

        return [reply_types.Input.from_dict(item) for item in reply]
