# pylint: disable=logging-fstring-interpolation

from typing import Any, Callable, Iterable

import attr


class FromDictMixin:
    @classmethod
    def from_dict(cls, data):
        return attrs_from_dict(cls, data)


def attrs_from_dict(cls, data: dict):
    def _fields() -> Iterable[tuple[str, Any]]:
        """
        attr.ib(default=None, factory=list, metadata={'key': 'someotherkey'})
        """

        field: attr.Attribute

        for field in attr.fields(cls):
            try:
                key = field.metadata["key"]
            except KeyError:
                key = field.name

            if field.default == attr.NOTHING:
                # rquired
                try:
                    val = data[key]
                except KeyError as e:
                    raise ValueError(f"invalid data to create {cls}") from e
                else:
                    yield field.name, val
            else:
                # optional
                try:
                    val = data[key]
                except KeyError:
                    continue
                else:
                    yield field.name, val

    return cls(**dict(_fields()))


def attrs_or_none(creator: Callable):
    def create(data):
        if data is None:
            return None

        return creator(data)

    return create
