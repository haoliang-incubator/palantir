"""
see: https://i3wm.org/docs/ipc.html
see: /usr/include/i3/ipc.h

some codes were stolen from python-i3ipc
"""

import struct
from enum import Enum
from typing import Union

import attr


class HeaderStruct:
    MAGIC = b"i3-ipc"
    FORMAT = f"={len(MAGIC)}sII"
    SIZE = struct.calcsize(FORMAT)


class MessageType(Enum):
    COMMAND = 0
    GET_WORKSPACES = 1
    SUBSCRIBE = 2
    GET_OUTPUTS = 3
    GET_TREE = 4
    GET_MARKS = 5
    GET_BAR_CONFIG = 6
    GET_VERSION = 7
    GET_BINDING_MODES = 8
    GET_CONFIG = 9
    SEND_TICK = 10
    GET_BINDING_STATE = 12
    # sway-specific command types
    GET_INPUTS = 100
    GET_SEATS = 101


class ReplyType(Enum):
    COMMAND = 0
    WORKSPACES = 1
    SUBSCRIBE = 2
    OUTPUTS = 3
    TREE = 4
    MARKS = 5
    BAR_CONFIG = 6
    VERSION = 7
    BINDING_MODES = 8
    CONFIG = 9
    TICK = 10
    BINDING_STATE = 12
    # sway-specific
    GET_INPUTS = 100
    GET_SEATS = 101


class EventType(Enum):
    WORKSPACE = 1 << 0
    OUTPUT = 1 << 1
    MODE = 1 << 2
    WINDOW = 1 << 3
    BARCONFIG_UPDATE = 1 << 4
    BINDING = 1 << 5
    SHUTDOWN = 1 << 6
    TICK = 1 << 7
    # sway-specific
    INPUT = 1 << 21

    def to_string(self):
        return str.lower(self.name)

    @staticmethod
    def from_string(val):
        match = [e for e in EventType if e.to_string() == val]

        if not match:
            raise ValueError("event not implemented: " + val)

        return match[0]

    def to_list(self):
        events_list = []
        if self.value & EventType.WORKSPACE.value:
            events_list.append(EventType.WORKSPACE.to_string())
        if self.value & EventType.OUTPUT.value:
            events_list.append(EventType.OUTPUT.to_string())
        if self.value & EventType.MODE.value:
            events_list.append(EventType.MODE.to_string())
        if self.value & EventType.WINDOW.value:
            events_list.append(EventType.WINDOW.to_string())
        if self.value & EventType.BARCONFIG_UPDATE.value:
            events_list.append(EventType.BARCONFIG_UPDATE.to_string())
        if self.value & EventType.BINDING.value:
            events_list.append(EventType.BINDING.to_string())
        if self.value & EventType.SHUTDOWN.value:
            events_list.append(EventType.SHUTDOWN.to_string())
        if self.value & EventType.TICK.value:
            events_list.append(EventType.TICK.to_string())
        if self.value & EventType.INPUT.value:
            events_list.append(EventType.INPUT.to_string())

        return events_list


@attr.s(kw_only=True)
class Header:
    length: int = attr.ib()
    type: Union[MessageType, EventType, ReplyType] = attr.ib()
    magic: bytes = attr.ib(default=HeaderStruct.MAGIC)

    @classmethod
    def from_bytes(cls, data: Union[bytes, bytearray]):
        assert len(data) == HeaderStruct.SIZE
        magic, length, _type = struct.unpack(HeaderStruct.FORMAT, data)  # type: ignore
        assert magic == HeaderStruct.MAGIC

        type: Union[EventType, ReplyType]

        try:
            # see https://i3wm.org/docs/ipc.html#_available_events
            if _type >> 31:
                type = EventType(1 << (_type & 0x7F))
            else:
                type = ReplyType(_type)
        except AttributeError as e:
            raise RuntimeError("unsupported message type") from e
        else:
            return cls(magic=magic, length=length, type=type)

    def pack(self) -> bytes:
        return struct.pack(
            HeaderStruct.FORMAT, HeaderStruct.MAGIC, self.length, self.type.value
        )
