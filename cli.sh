#!/usr/bin/env bash

set -e

readonly PROJ=$(dirname $(realpath $0))

export PYTHONPATH=$PROJ

exec $PROJ/venv/bin/python -m palantir.cli --quiet "$@"
