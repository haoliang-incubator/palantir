#!/usr/bin/env bash

readonly fifo=$XDG_RUNTIME_DIR/palantir.fifo

test -p $fifo || {
    >&2 echo "invalid FIFO of palantir: ${fifo}"
    exit 1
}

echo $@ > $fifo
