
### purpose

customized i3 flow:
* switch/jump in workspaces using wpno
* rename current workspace, and following wpid naming convention
* move container to specific workspace using wpno
* rename current/specific workspace name with running program icons, and following wpid naming convention


### terms

* wp: [w]orks[p]ace; I personally prefer `wp` than `ws`
* wpid: `no:name`
    * no: [0-9] | [a-z]
    * ord: ord(no)
    * name: purpose


### usage

daemon-mode
1) install palantir.service via `python -m palantir.systemd_service install`
2) start palantir.service via `systemctl --user start palantir`
3) run commands via `./fifo.sh`, eg. `./fifo.sh jumpto 1`

cli-mode
1) run commands via `./cli.sh`, eg. `./fifo.sh jumpto 1`

available commands: see `./cli.sh --help`


### task-clock comparation

according to perf.stat

* daemon-mode = 4.56 + 6.84/2 ~= 8
* cli-mode = 171.85

~ 20x


---

about `palantir` the name
* see https://en.wikipedia.org/wiki/Palantír

